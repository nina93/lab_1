__author__ = 'Justyna'

import xml.etree.ElementTree as ET
import re

class XmlParser:
    l = list()

    def __init__(self, fileName):
        self.file = fileName

    def parse(self):
        tree = ET.parse(self.file)
        root = tree.getroot()

        for book in root.findall('book'):
            t = ()
            t = t + (book.get('id'),)
            for i,val in enumerate(book):
                text = val.text
                if '\n' in text or re.compile(r'\s+').search(text) is not None:
                    description = text.split('\n')
                    descList = []
                    descriptionStr = ''

                    for d in description:
                        desc = re.sub('\s+', ' ', d)
                        if d:
                            descList.append(desc)

                    for i,value in enumerate(descList):
                        descriptionStr += value
                    t = t + (re.sub('\s+', ' ', descriptionStr),)
                else:
                    t = t +(text,)

            self.l.append(t)

    def __str__(self):
        return str(self.l)

    def getList(self):
        return self.l

if __name__ == '__main__':
    books = XmlParser('Books.xml')
    books.parse()
    print books



from unittest import TestCase
from XmlParser import XmlParser

__author__ = 'Justyna'


class TestXmlParser(TestCase):
    count = 1

    def test_parse(self):
        for i in range(self.count):
            object = XmlParser('Test' + str(i) + '.xml')
            XmlResult = "[('1', 'Core first', 'Core second')]"
            object.parse()
            self.assertEqual(XmlResult, str(object.getList()))
